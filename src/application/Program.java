package application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import model.entities.Reservation;

public class Program {
	public static void main(String[] args) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Scanner sc = new Scanner(System.in);

		System.out.print("N�mero do quarto: ");
		int number = sc.nextInt();
		System.out.print("Informe a data de entrada (dd/MM/yyyy)");
		Date checkIn = sdf.parse(sc.next());
		System.out.print("Informe a data de sa�da (dd/MM/yyyy)");
		Date checkOut = sdf.parse(sc.next());

		if (!checkOut.after(checkIn)) {
			System.out.println("Ocorreu um erro. A data de sa�da n�o pode ser inferior a data de entrada.");
		} else {
			Reservation reservation = new Reservation(number, checkIn, checkOut);
			System.out.println("Reserva: " + reservation);

			System.out.println();
			
			System.out.println("Deseja alterar as datas? (Y/N)");
			char option = sc.next().charAt(0);

			if (option == 'y' || option == 'Y') {

				// atualizando as datas de entrada e sa�da
				System.out.print("Informe a nova data de entrada (dd/MM/yyyy)");
				checkIn = sdf.parse(sc.next());
				System.out.print("Informe a nova data de sa�da (dd/MM/yyyy)");
				checkOut = sdf.parse(sc.next());

				Date now = new Date();

				if (checkIn.before(now) || checkOut.before(now)) {
					System.out.print("As datas inseridas s�o inv�lidas!");
				}
				// se a data de sa�da n�o for posterior a data de checkin n�o � aceit�vel
				else if (!checkOut.after(checkIn)) {
					System.out.print("Erro na reserva. A data de sa�da tem que ser depois da data de entrada.");
				} else {
					reservation.updateDates(checkIn, checkOut);
				}

			} else if (option == 'n' || option == 'N') {
				System.out.println("Opera��o finalizada!");
			}
		}
	}
}
